#!/usr/bin/python2
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    A copy of the GNU General Public License is available at
#    http://www.gnu.org/licenses/gpl-3.0.html

"""Generate subset from count file"""

import os
import sys
import random
from argparse import ArgumentParser


__author__ = "Mathieu Almeida"
__copyright__ = "Copyright 2022, INRAE"
__license__ = "GPL"
__version__ = "1.0.0"
__maintainer__ = "Mathieu Almeida"
__email__ = "mathieu.almeida@inrae.fr"
__status__ = "Developpement"


def isfile(path):
    """Check if path is an existing file.
      :Parameters:
          path: Path to the file
    """
    if not os.path.isfile(path):
        if os.path.isdir(path):
            msg = "{0} is a directory".format(path)
        else:
            msg = "{0} does not exist.".format(path)
        raise argparse.ArgumentTypeError(msg)
    return path



#===================
#parameters
#===================
def config_parameters():
    """Extract program options
    """
    parser = ArgumentParser(description=__doc__,
                            usage="{0} -h [options] [arg]".format(sys.argv[0]))
    parser.add_argument('-f', '--countfile', dest='countfile', type=isfile,
                        required=True, help='Input fasta file. Warning : only *.fasta')
    parser.add_argument('-r', '--reffile', dest='reffile', type=isfile,
                        required=True, help='Input reference file')
    parser.add_argument('-o', '--outsubset', dest='outfile', type=str,
                        default=None, help='Define a sample name')
    parser.add_argument('-t', '--subsetthreshold', dest='subsetthreshold', type=list,
                        default=[10000,100000,200000,300000,400000,500000,600000], 
                        help='Subset threshold count')
    parser.add_argument('-i', '--iteration', dest='iteration', type=int,
                        default=3, help='Iteration subset')
                                                    
    return parser.parse_args()


#===========================================
#remove contigs or scaffolds shorter than trimSize
#===========================================
def make_subset_file(CountFileName, RefFileName, 
   SubsetThresholdList, OutFileName, Iteration):
    """Make subset count file
    """
    ref_count_subset={}
    ref_detected=[]
    with open(RefFileName, "rt") as RefFile:
       for line in RefFile:
          ref_count_subset[line[1:].strip()] = 0
    
    with open(CountFileName, "rt") as CountFile:
       for line in CountFile:
          items = line.strip().split()
          ref = items[0]
          
          count= int(items[1])
          for i in range(count):
             ref_detected.append(ref)
    
    #subset
    maxcount = len(ref_detected)
    
    for iter in range(1,Iteration+1):
       for threshold in SubsetThresholdList:
          for ref in ref_count_subset:
             ref_count_subset[ref] = 0
          
          list_rank = [x for x in range(maxcount)]  
          list_rank_selected = random.sample(list_rank, int(threshold))

          for rank in list_rank_selected:
              ref = ref_detected[int(rank)]
              ref_count_subset[ref]+=1
        
          with open(OutFileName + "_" + str(threshold) \
             + "_" + str(iter) + ".txt", "wt") as OutputFile:
             for ref in sorted(ref_count_subset):
                OutputFile.write("%s\t%i\n" % (ref, ref_count_subset[ref]) )
          
        
#===================
#MAIN
#===================
def main():
    """Main function
    """
    args = config_parameters()
    
    make_subset_file(args.countfile, args.reffile, 
       args.subsetthreshold, args.outfile, args.iteration)


if __name__=="__main__":
    main()
