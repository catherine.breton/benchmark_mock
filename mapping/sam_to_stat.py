#!/usr/bin/env python
# -*- coding: utf-8 -*-
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    A copy of the GNU General Public License is available at
#    http://www.gnu.org/licenses/gpl-3.0.html

"""Convert sam file into read error profile per reference genome"""

import argparse
import sys
import re
import os

__author__ = "Mathieu Almeida"
__copyright__ = "Copyright 2022, INRAE"
__license__ = "GPL"
__version__ = "0.0.0"
__email__ = "mathieu.almeida@inrae.fr"
__status__ = "Developpement"


#======================
#check file
#======================
def isfile(path):
    """Check if path is an existing file.
       :Parameters:
          path: Path to the file
    """
    if not os.path.isfile(path):
       if os.path.isdir(path):
          msg = "{0} is a directory".format(path)
       else:
          msg = "{0} does not exist.".format(path)
       raise argparse.ArgumentTypeError(msg)
    return path


#=====================
#get params
#=====================
def get_opts():
   """Extract program options
   """
   parser = argparse.ArgumentParser(description=__doc__,
            usage="{0} -h [options] [arg]".format(sys.argv[0]))
    
   parser.add_argument('-i', '--samfile',
          dest='sam_file', type=isfile, required=True,
          help='Input SAM file bowtie output like')
   parser.add_argument('-o', '--outfile',
          dest='out_file', type=str, required=True,
          help='Output stat file')
   parser.add_argument('-s', '--seqtechno',
          dest='seq_techno', type=str, required=True,
          help='Sequencing techno used')      
   return parser.parse_args()            


#============================
#sam to stat bowtie2 SAM file
#============================
def sam_to_stat_short(SamFileName, OutFileName):
   #init
   rejected_flag=["4","77","141","165","101","133","69"]

   with open(SamFileName, "rt") as samfile:
      with open(OutFileName, "wt") as outfile:
 
         #extract best-hits
         outfile.write("genome percIdentity percIndels percSNPs\n")

         for line in samfile:
            if line[0]!='@':

               items = line.split('\t')
               read=items[0]
               flag=items[1]
               genome=items[2].split('|')[0]
               CIGAR=items[5]
               read_length = len(items[9].strip())
               
               if flag not in rejected_flag:
                  
                  match_len=0
                  indel=0
                  subs=0
                  edit_distance=0
                  perc_identity = 0.0
                  perc_indel = 0.0
                  perc_subs = 0.0
                  
                  #search and extract edit distance
                  for i in range(len(items[15:])):
                     if "NM" in items[i+15]:
                        edit_distance = int(items[i+15].replace("NM:i:", ""))
                  
                  CIGAR_list = filter(None, re.split(r'(\d+)', CIGAR))
                  for i in range(len(CIGAR_list)-1):
                     if CIGAR_list[i+1]=="M":
                        match_len += int(CIGAR_list[i])
                     elif CIGAR_list[i+1]=="I" or CIGAR_list[i+1]=="D":
                        indel += int(CIGAR_list[i])
                  
                  subs = edit_distance - indel
                  
                  perc_identity = (1 - ( edit_distance / float(read_length))) * 100
                  perc_indel = (indel / float(read_length))*100
                  perc_subs = (subs / float(read_length))*100
                  
                  outfile.write( genome + " " +str(perc_identity) + " " + str(perc_indel) + " "+ str(perc_subs) + "\n")
   
   print("end sam_to_stat_short")


#============================
#sam to stat minimap SAM file
#============================
def sam_to_stat_long(SamFileName, OutFileName):
   rejected_flag=["4","2064","2048","256","272"]

   with open(SamFileName, "rt") as samfile:
      with open(OutFileName, "wt") as outfile:
         
         #extract best-hits
         outfile.write("genome percIdentity percIndels percSNPs\n")
         
         for line in samfile:
            if line[0]!='@':
               
               items = line.split('\t')
               read=items[0]
               flag=items[1]
               genome=items[2].split('|')[0]
               CIGAR=items[5]
               read_length = len(items[9].strip())
               
               if flag not in rejected_flag:
                  
                  match_len=0
                  soft_clipping_len=0
                  indel=0
                  subs=0
                  edit_distance=0
                  perc_identity = 0.0
                  perc_indel = 0.0
                  perc_subs = 0.0
                  
                  #search and extract edit distance
                  for i in range(len(items[10:])):
                     if "NM:i:" in items[i+10][0:5]:
                        edit_distance = int(items[i+10].replace("NM:i:", ""))
                  
                  CIGAR_list = filter(None, re.split(r'(\d+)', CIGAR))
                  for i in range(len(CIGAR_list)-1):
                     if CIGAR_list[i+1]=="M":
                        match_len += int(CIGAR_list[i])
                     elif CIGAR_list[i+1]=="I" or CIGAR_list[i+1]=="D":
                        indel += int(CIGAR_list[i])
                     elif CIGAR_list[i+1]=="S":
                        soft_clipping_len+=int(CIGAR_list[i])
                  subs = edit_distance - indel
                  
                  perc_identity = (1 - ( edit_distance / float(read_length - soft_clipping_len))) * 100
                  perc_indel = (indel /float(read_length - soft_clipping_len))*100
                  perc_subs = (subs/float(read_length  - soft_clipping_len))*100
                  outfile.write( genome + " " +str(perc_identity) + " " + str(perc_indel) + " "+ str(perc_subs) + "\n")
   
   print("end sam_to_stat_long")


#====================================
#MAIN
#====================================
def main():
   """Main program function
   """

   #init
   short_read_techno=["illumina","proton","s5","mgiseq_2000", "mgiseq_t7"]
   long_read_techno=["pacbio","minion"]
   #Extract arguments
   opts = get_opts()
     
   #convert sam to stat file
   if opts.seq_techno in short_read_techno:
      print("start short read stats...")
      sam_to_stat_short(opts.sam_file, opts.out_file)
   elif opts.seq_techno in long_read_techno:
      print("start long read stats...")
      sam_to_stat_long(opts.sam_file, opts.out_file)
   
   print("end MAIN")

if __name__ == '__main__':
   main()
