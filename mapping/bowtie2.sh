#!/usr/bin/bash

sample="MOCK_001"
techno="illumina"

bowtie2-build ${sample}.fasta ${sample}.bt2

#if PE reads
bowtie2 -x ${sample}.bt2 -1 ${sample}_1.fastq -2 ${sample}_2.fastq --end-to-end -p 8 --no-unal --no-sq --no-head -S ${sample}.sam &> ${sample}.log

#if SE reads
bowtie2 -x ${sample}.bt2 -U ${sample}.fastq --end-to-end -p 8 --no-unal --no-sq --no-head -S ${sample}.sam &> ${sample}.log

python2 sam_to_stat.py -i ${sample}.${techno}.sam -o ${sample}.${techno}.stat -s ${techno}
