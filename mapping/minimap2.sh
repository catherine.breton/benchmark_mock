#!/usr/bin/bash

sample="MOCK_001"
techno="pacbio"

minimap2 -x map-pb -Y --secondary=no -k 15 -a -o ${sample}.sam -t 12 ${sample}.fna ${sample}.fastq

python2 sam_to_stat.py -i ${sample}.${techno}.sam -o ${sample}.${techno}.stat -s ${techno}

