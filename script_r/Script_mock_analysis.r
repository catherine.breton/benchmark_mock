##### Authors  #####
# Victoria Meslier #
#  Kévin Da Silva  #
####################

library(ggplot2)
library(ggpubr)
library(reshape2)
library(stringr)
library(data.table)
library("xlsx")

# process subsampled files to get count data

list_of_files = list.files(path = "benchmark_mock/profiling/subset", recursive = TRUE, pattern = "mock", full.names = TRUE)
data = rbindlist(sapply(list_of_files, fread, simplify = FALSE), use.names = TRUE, idcol = "FileName")
colnames(data) = c("full_name", "genome", "count")
data$full_name = gsub(".*\\/(.*_.*_.*_[0-9]).txt","\\1",data$full_name) # delete path and extension, keep only mock#_tech_subsamp_iter
data = cbind(data,(do.call(rbind, strsplit(as.vector(data$full_name), split = "_"))))
colnames(data)[4:7] = c("mock", "technology", "depth", "iteration")

data$technology = gsub("bgig400","dnbseq-g400",data$technology) # change tech name
data$technology = gsub("bgit7","dnbseq-t7",data$technology) # change tech name
data$full_name = gsub("bgig400","dnbseq-g400",data$full_name) # change tech name
data$full_name = gsub("bgit7","dnbseq-t7",data$full_name) # change tech name

# save(data, file = "./subsampling_raw_counts_mock_data.RData")
# load(file = "./subsampling_raw_counts_mock_data.RData")

# loading mock composition

data_genome = read.xlsx("./Supplementary_Table_S1.xlsx", 1, header=TRUE, stringsAsFactors=F) 
data_genome = data_genome[1:91,1:18] # remove NA lines and columns
data_genome = data_genome[,-grep("^Phylum.NCBI$",colnames(data_genome))] # keep only Phylum GTDB column for Phylum info
colnames(data_genome) = c("Organism_Name", "Strain_Name", "Genome_label", "Genbank_Assembly_Accession_ID", "Kingdom", "Phylum", "Genome_Size_Mbp", "#_scaffolds", "#_Ns", "Genome_Status","Notes", "GC_perc", "#_rRNA_operons", "mock1_perc", "mock2_perc", "mock3_perc", "phylogenetic_position")
data_genome[,"Genome_Size_Mbp"] = as.numeric(data_genome[,"Genome_Size_Mbp"]) # character to numeric
data_genome$mock1_perc = as.numeric(data_genome$mock1_perc) # character to numeric
data_genome$mock1_perc[is.na(data_genome$mock1_perc)] = 0
data_genome$mock2_perc = as.numeric(data_genome$mock2_perc) # character to numeric
data_genome$mock2_perc[is.na(data_genome$mock2_perc)] = 0
data_genome$mock3_perc = as.numeric(data_genome$mock3_perc) # character to numeric
data_genome$mock3_perc[is.na(data_genome$mock3_perc)] = 0

# compute data abundance (normalized by genome size and as relative abundance)

data$norm_count = data$count/data_genome[match(data$genome, data_genome$Genbank_Assembly_Accession_ID), "Genome_Size_Mbp"]

abundance = as.data.frame(dcast(data, genome~full_name, value.var = "norm_count")) # normalized counts
rownames(abundance) = abundance$genome
abundance = abundance[,-1]
abundance = apply(abundance,2,function(x) x/sum(x,na.rm=T)*100) # relative abundance
abundance[is.na(abundance)] = 0 # replace NAs by 0

####################################################################
## Spearman correlation between expected and observed composition 
####################################################################

# compute spearman correlation

cor_results = setNames(data.frame(matrix(ncol = 6, nrow = 0)), c("mock", "technology", "subsampling","iteration","rho","pval"))

for (sample in colnames(abundance)) {
  name = strsplit(sample,"_")[[1]]
  cor_res = cor.test(x= data_genome[match(rownames(abundance), data_genome$Genbank_Assembly_Accession_ID),paste(name[1],"perc",sep="_")], y = abundance[,sample], method="spearman")
  cor_results[nrow(cor_results)+1,] = c(name,cor_res$estimate,cor_res$p.value)
}
cor_results$rho = as.numeric(cor_results$rho)
cor_results$pval = as.numeric(cor_results$pval)

# mean and sd determination on spearman rho

mean_rho = aggregate(x = cor_results$rho, by = list(cor_results$mock, cor_results$technology, cor_results$subsampling), FUN = mean)
colnames(mean_rho) = c("mock", "technology", "depth", "mean")

sd_rho = aggregate(x = cor_results$rho, by = list(cor_results$mock, cor_results$technology, cor_results$subsampling), FUN = sd)
colnames(sd_rho) = c("mock", "technology", "depth", "sd")

# data to plot
data_to_plot = cbind(mean_rho, sd=sd_rho$sd)
data_to_plot$label[data_to_plot$mock == "mock1"] = "Mock 1 - 71 species"
data_to_plot$label[data_to_plot$mock == "mock2"] = "Mock 2 - 87 species"
data_to_plot$label[data_to_plot$mock == "mock3"] = "Mock 3 - 64 species"
data_to_plot$depth = factor(data_to_plot$depth, levels = c("10000","100000","200000","300000","400000","500000","600000","700000","800000","900000","1000000"))
data_to_plot$label = factor(data_to_plot$label, levels =c("Mock 3 - 64 species", "Mock 1 - 71 species", "Mock 2 - 87 species"))

## plot 
tiff(file = "./Figure2.tiff", height = 4, width = 8, units = "in", res = 250)
ggplot(data=data_to_plot, aes(x = depth, y = mean, group = technology, color = technology))+
  geom_line()+
  geom_point()+
  geom_errorbar(aes(ymin=mean-sd, ymax=mean+sd), width=.2, position=position_dodge(0.05))+
  theme_classic()+
  theme(axis.text.x = element_text(angle = 90, vjust=0.5))+
  scale_color_manual(values=c("#b30000","#ff0000","#009933",  "#ff9933", "#9900cc", "#99ccff", "#0066ff"))+
  scale_x_discrete(labels=c("10k", "100k", "200k", "300k", "400k", "500k", "600k", "700k", "800k", "900k", "1m"))+
  ylab("Spearman rho (mean +/- sd) on normalized counts")+
  facet_wrap(.~ label)
dev.off()


####################################################################
## Differential analysis MOCK1 to expected composition
####################################################################

data_diff_mock1 = as.data.frame(abundance[,grep("mock1_",colnames(abundance))]-data_genome$mock1_perc)
data_diff_mock1$genome = rownames(data_diff_mock1)
data_diff_mock1 = reshape2::melt(data_diff_mock1, id.vars = "genome")
data_diff_mock1 = cbind(data_diff_mock1,data_genome[match(data_diff_mock1$genome, data_genome$Genbank_Assembly_Accession_ID), c("Genome_Size_Mbp","GC_perc","Genome_label","phylogenetic_position")])
data_diff_mock1 = cbind(data_diff_mock1, str_split_fixed(data_diff_mock1$variable, "_", 4))  
colnames(data_diff_mock1)[8:11] = c("mock", "technology", "depth", "iteration")

data_500k = data_diff_mock1[which(data_diff_mock1$depth == "500000"),]
data_500k$genome = factor(data_500k$genome)

# organizing data_500k by value
data_500k$genome = with(data_500k, reorder(genome, phylogenetic_position))
data_500k = data_500k[order(data_500k$phylogenetic_position),]

#creating color df for x labels | black = complete genome | red = draft genome
col = as.data.frame(ifelse(data_genome$Genome_Status == "Complete", "black", "firebrick"))
colnames(col) = "color"
col$genome = data_genome$Genbank_Assembly_Accession_ID
col = col[match(unique(data_500k$genome), col$genome),]

## plot 
a = ggplot(data=data_500k, aes(x=genome, y=value, color=technology)) +
  geom_point() +
  theme_classic()+
  geom_hline(yintercept = 0)+
  theme(axis.text.x = element_text(angle = 90, vjust=0.5, hjust = 1, size =6, colour = col$color))+
  scale_x_discrete(labels = unique(data_500k$Genome_label))+
  scale_color_manual(values=c("#b30000","#ff0000","#009933",  "#ff9933", "#9900cc","#99ccff", "#0066ff"))+
  ylab("Difference to expected composition")+
  xlab("genome ordered by phylogenetic position") +
  theme(legend.position="bottom")+
  theme(axis.title.y = element_text(size=7))
b = ggplot(data_500k, aes(x = genome, y = Genome_Size_Mbp))+
  geom_point(color = "#339966")+
  theme_classic()+
  ylab("genome size")+
  theme(axis.title.x = element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank())+
  theme(axis.title.y = element_text(size=7))
c = ggplot(data_500k, aes(x = genome, y = as.numeric(GC_perc)))+
  geom_point(color = "#666699")+
  theme_classic()+
  ylab("GC %")+
  theme(axis.title.x = element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank())+
  theme(axis.title.y = element_text(size=7))
tiff(file = "./Figure3.tiff", height = 7, width = 8, units = "in", res = 250)
ggarrange(b,c,a, nrow = 3, ncol =1, align = "v", heights = c(0.3,0.3,2.5))
dev.off()
