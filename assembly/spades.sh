#!/usr/bin/bash

sample="MOCK_001"

#if PE reads
techno="illumina"
spades.py --meta -1 ${sample}_1.fastq -2 ${sample}_2.fastq -o ${sample}.${techno} -k 21,33,55,77 --threads 12 -m 90


#if SE reads
techno="proton"
spades.py --careful --iontorrent -s ${sample}.fastq -o ${sample}.${techno} -k 21,33,55,77 --threads 12



