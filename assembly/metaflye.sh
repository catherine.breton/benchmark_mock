#!/usr/bin/bash

sample="MOCK_001"

#for pacbio
techno="pacbio"
flye --pacbio-hifi ${sample}.fastq -t 12 --meta --plasmids --min-overlap 2000 --hifi-error 0.003 -o metaflye_${sample}_${techno}

#for minion
techno="minion"
flye --nano-raw ${sample}.fastq -t 12 --meta --plasmids --min-overlap 2000 -o metaflye_${sample}_${techno}
