All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    scaffolds  scaffolds_broken
# contigs (>= 1000 bp)      15         13              
# contigs (>= 5000 bp)      1          1               
# contigs (>= 10000 bp)     0          0               
# contigs (>= 25000 bp)     0          0               
# contigs (>= 50000 bp)     0          0               
Total length (>= 1000 bp)   28348      25660           
Total length (>= 5000 bp)   6579       6000            
Total length (>= 10000 bp)  0          0               
Total length (>= 25000 bp)  0          0               
Total length (>= 50000 bp)  0          0               
# contigs                   859        735             
Largest contig              6579       6000            
Total length                505472     426856          
GC (%)                      55.25      55.73           
N50                         532        525             
N75                         508        505             
L50                         364        315             
L75                         608        523             
# N's per 100 kbp           367.18     0.00            
