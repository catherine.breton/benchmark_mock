Assemblies                                           scaffolds  scaffolds_broken
Acidilobus_saccharovorans_345_15                     -          -               
Acidobacterium_capsulatum_ATCC_51196                 5402       5402            
Aciduliprofundum_boonei_T469                         -          -               
Akkermansia_muciniphila_ATCC_BAA_835                 17649      17649           
Archaeoglobus_fulgidus_DSM_4304                      10863      10863           
Bacteroides_thetaiotaomicron_VPI_5482                3553       3553            
Bacteroides_vulgatus_ATCC_8482                       13242      13242           
Bordetella_bronchiseptica_RB50                       2284       2284            
Caldicellulosiruptor_bescii_DSM_6725                 7923       7923            
Caldicellulosiruptor_saccharolyticus_DSM_8903        155040     155040          
Chlorobium_limicola_DSM_245                          133530     133530          
Chlorobium_phaeobacteroides_DSM_266                  54240      54240           
Chlorobium_phaeovibrioides_DSM_265                   281990     281990          
Chlorobium_tepidum_TLS                               38140      38140           
Chloroflexus_aurantiacus_J_10_fl                     11420      11420           
Deinococcus_radiodurans_R1                           2641       2641            
Desulfitobacterium_dehalogenans_ATCC_51507           -          -               
Desulfobulbus_oralis_sp._ONRL                        61473      61473           
Desulfovibrio_desulfuricans_ND132                    -          -               
Desulfovibrio_piger_ATCC_29098                       10758      10758           
Desulfovibrio_vulgaris_Hildenborough                 -          -               
Dictyoglomus_turgidum_DSM_6724                       997        997             
Enterococcus_faecalis_OG1RF                          1335       1335            
Fusobacterium_nucleatum_subsp._nucleatum_ATCC_25586  24137      24137           
Gemmatimonas_aurantiaca_T_27                         110667     110667          
Geobacter_bemidjiensis_Bem                           -          -               
Geobacter_sulfurreducens_PCA                         6200       6200            
Halobacterium_salinarum_NRC_1                        30983      30983           
Haloferax_volcanii_DS2                               -          -               
Herpetosiphon_aurantiacus_DSM_785                    220136     220136          
Hydrogenobaculum_sp._Y04AAS1                         34820      34820           
Ignicoccus_hospitalis_KIN4_I                         90681      90681           
Ignicoccus_islandicus_DSM_13165                      115900     115900          
Leptothrix_cholodnii_SP_6                            565        565             
Methanobrevibacter_oralis_DSM_7256                   52699      52699           
Methanocaldococcus_jannaschii_DSM_2661               127975     127975          
Methanococcus_maripaludis_C5                         37822      37822           
Methanococcus_maripaludis_S2                         3516       3516            
Methanomassiliicoccus_luminyensis_B10                586        586             
Methanomethylovorans_hollandica_DSM_15978            988        988             
Methanopyrus_kandleri_AV19                           12663      12663           
Methanosarcina_acetivorans_C2A                       1041       1041            
Methanothermus_fervidus_DSM_2088                     110000     110000          
Nanoarchaeum_equitans_Kin4_M                         23888      23888           
Nitrosomonas_europaea_ATCC_19718                     158327     158327          
Nostoc_sp._PCC_7120                                  86840      86840           
Paraburkholderia_xenovorans_LB400                    12508      12508           
Persephonella_marina_EX_H1                           384996     384996          
Porphyromonas_gingivalis_ATCC_33277                  125905     125905          
Pseudomonas_fluorescens_SBW25                        39760      39760           
Pseudomonas_putida_KT2440                            26324      26324           
Pyrobaculum_aerophilum_IM2                           996        996             
Pyrobaculum_arsenaticum_DSM_13514                    556        556             
Pyrobaculum_calidifontis_JCM_11548                   24006      24006           
Pyrococcus_furiosus_DSM_3638                         3787       3787            
Pyrococcus_horikoshii_OT3                            296754     296754          
Rhodopirellula_baltica_SH_1                          238042     238042          
Ruegeria_pomeroyi_DSS_3                              639        639             
Ruminiclostridium_thermocellum_ATCC_27405            7093       7093            
Salinispora_arenicola_CNS_205                        1884       1884            
Salinispora_tropica_CNB_440                          1551       1551            
Shewanella_baltica_OS185                             88136      88136           
Shewanella_baltica_OS223                             114274     114274          
Shewanella_loihica_PV_4                              7376       7376            
Sulfolobus_tokodaii_str._7                           1318       1318            
Sulfurihydrogenibium_sp._YO3AOP1                     4381       4381            
Thermoanaerobacter_pseudethanolicus_ATCC_33223       341377     341377          
Thermotoga_neapolitana_DSM_4359                      268869     268869          
Thermotoga_petrophila_RKU_1                          202254     202254          
Treponema_denticola_ATCC_35405                       17994      17994           
Wolinella_succinogenes_DSM_1740                      24530      24530           
