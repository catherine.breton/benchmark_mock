All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    scaffolds  scaffolds_broken
# contigs (>= 1000 bp)      49         49              
# contigs (>= 5000 bp)      0          0               
# contigs (>= 10000 bp)     0          0               
# contigs (>= 25000 bp)     0          0               
# contigs (>= 50000 bp)     0          0               
Total length (>= 1000 bp)   69209      69209           
Total length (>= 5000 bp)   0          0               
Total length (>= 10000 bp)  0          0               
Total length (>= 25000 bp)  0          0               
Total length (>= 50000 bp)  0          0               
# contigs                   1339       1339            
Largest contig              4102       4102            
Total length                821545     821545          
GC (%)                      57.29      57.29           
N50                         566        566             
N75                         511        511             
L50                         544        544             
L75                         932        932             
# N's per 100 kbp           0.00       0.00            
